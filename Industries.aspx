﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Industries.aspx.cs" Inherits="Industries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="center wow fadeInDown" style="background: #12a5f4; padding: 1%; box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
        <div id="background-wrap" style="height: 0px;">
            <div class="x4">
                <div class="cloud"></div>
            </div>
        </div>
        <h2 style="color: #fff;">Industries</h2>
        <p class="lead" style="color: #FFF;">Proven track record of improving our client’s brand, capture new and loyal customers across VARIOUS INDUSTRIES</p>
    </div>

    <section id="content" style="margin-top: 3%;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 wow fadeInDown">
                    <div class="tab-wrap">
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">RETAIL</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">AUTOMOBILE</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">REAL ESTATE</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">CONVENTIONS</a></li>
                                    <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">HOSPITALITY & RESTAURANT</a></li>


                                    <li class=""><a href="#tab6" data-toggle="tab" class="tehnical">HEALTHCARE</a></li>
                                    <li class=""><a href="#tab7" data-toggle="tab" class="tehnical">TOURISM</a></li>
                                    <li class=""><a href="#tab8" data-toggle="tab" class="tehnical">EDUCATION</a></li>

                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">

                                    <div class="tab-pane fade active in" id="tab1">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind1.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Compare & Select Products</p>
                                                <p>See Products in Other Branches</p>
                                                <p>Virtual Trial & Match</p>
                                                <p>Product specific accessories</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab2">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind2.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Vehicle Customizations</p>
                                                <p>EMI Calculations</p>
                                                <p>Email Digital catalogue</p>
                                                <p>Compare across variants</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab3">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind3.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>3D views</p>
                                                <p>Select & Book</p>
                                                <p>Email catalogue and agreement</p>
                                                <p>Compare spaces</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab4">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind4.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Browse Entire Inventory</p>
                                                <p>Plug and Play</p>
                                                <p>Create and Email Catalogue</p>
                                                <p>Center text:</p>
                                                <p>Showcase products with specifications</p>
                                                <p>Push Promotion & Deals</p>
                                                <p>Capture Customer data and preferences</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab5">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind5.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Digital Service card / Menu</p>
                                                <p>Customize food / beverages</p>
                                                <p>Digital Signage</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab6">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind6.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Digital Prescription</p>
                                                <p>Electronic Medical Records</p>
                                                <p>Understand Medical Procedure</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab7">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind7.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Browse locations</p>
                                                <p>Plan Visits & share on FB</p>
                                                <p>Email Postcards</p>
                                                <p>Get specific information</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab8">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="img-responsive" src="images/industries/ind8.png" alt="" />
                                            </div>
                                            <div class="media-body" style="float: left; margin-top: 1%;">
                                                <p>Digital board</p>
                                                <p>Create lessons, material</p>
                                                <p>Access from web</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/.tab-content-->
                            </div>
                            <!--/.media-body-->
                        </div>
                        <!--/.media-->
                    </div>
                    <!--/.tab-wrap-->
                </div>
                <!--/.col-sm-6-->

               <%-- <div class="col-xs-12 col-sm-4 wow fadeInDown">
                    <div class="testimonial">
                        <h2>Testimonials</h2>
                        <div class="media testimonial-inner">
                            <div class="pull-left">
                                <img class="img-responsive img-circle" src="images/testimonials1.png">
                            </div>
                            <div class="media-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                                <span><strong>-John Doe/</strong> Director of corlate.com</span>
                            </div>
                        </div>

                        <div class="media testimonial-inner">
                            <div class="pull-left">
                                <img class="img-responsive img-circle" src="images/testimonials1.png">
                            </div>
                            <div class="media-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                                <span><strong>-John Doe/</strong> Director of corlate.com</span>
                            </div>
                        </div>

                    </div>
                </div>--%>

            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <!--/#content-->

</asp:Content>


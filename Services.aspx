﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="center wow fadeInDown" style="background: #12a5f4; padding: 2%; box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
        <div id="background-wrap" style="height: 0px;">
            <div class="x4">
                <div class="cloud"></div>
            </div>
        </div>
        <h2 style="color: #fff;">Services</h2>
        <p class="lead" style="color: #FFF;"></p>
    </div>


       <div class="container" style="padding-top:4%; padding-bottom:4%;">
    <div class="team" style="padding-bottom:4%;">
				
				<div class="row clearfix">
					<div class="col-md-4 col-sm-6">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser1.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>Product Catalogue </h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Myfashions</a></li>
										<li class="btn"><a href="#">Touchoncloud</a></li>
										<li class="btn"><a href="#">MIoncloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Add your products onto the cloud; manage their images, description and prices in a matter of seconds. Manage inventory levels of each product across your outlets. </p>
                            <p>Decide where they must be published – web / mobile/ C touch. Easily update inventory on the cloud. Add once, and access on all platforms!</p>
						</div>
					</div><!--/.col-lg-4 -->
					
					
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser2.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>CRM</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">MIoncloud</a></li>
										<li class="btn"><a href="#">Touchoncloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Track and list all those potential customers who interacted on our platform and what products each of them are interested in. Also assign the potential customers to your sales team and add individual tasks to follow up</p>
						</div>
					</div><!--/.col-lg-4 -->					
				</div> <!--/.row -->

				<div class="row team-bar">
					<div class="first-one-arrow hidden-xs">
						<hr>
					</div>
					<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
					<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
				</div> <!--skill_border-->       

				<div class="row clearfix">   
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser3.png" alt=""/></a>
								</div>

								<div class="media-body">
									<h4>Promotions</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">MIoncloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Create promotions with a few clicks from your head office. Your promotions can be unique for locations and platforms - we have created the best solution to give you the flexibility to create and push promotions based on the C-Touch location(s).  Also create promotions specific to web / mobile / C-Touch</p>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 col-md-offset-2">
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser4.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>Personalized Workflows</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Myfashions</a></li>
										<li class="btn"><a href="#">Touchoncloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>We understand your brand is unique and we will do everything to customize all our solutions [both hardware and software] to be in-tune with your brand image and processes. </p>
						</div>
					</div>
				</div>	<!--/.row-->
			</div>


           <div class="team">
				
				<div class="row clearfix">
					<div class="col-md-4 col-sm-6">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser5.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>Business Intelligence</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">MIonCloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Get better insight into your customer’s shopping habits. Through our interactive surveys linked with business promotions, identify market trends even before your invest in production</p>
						</div>
					</div><!--/.col-lg-4 -->
					
					
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser6.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>Compare & Try</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Touchoncloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Our apps will let your customers browse through your entire products and services portfolio available across all your locations. Allow them to Select, Compare and Try with just a swipe or pinch on the touch screen</p>
						</div>
					</div><!--/.col-lg-4 -->					
				</div> <!--/.row -->
				<div class="row team-bar">
					<div class="first-one-arrow hidden-xs">
						<hr>
					</div>
					<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
					<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
				</div> <!--skill_border-->       

				<div class="row clearfix">   
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser7.png" alt=""/></a>
								</div>

								<div class="media-body">
									<h4>Screen Share on air</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">MIonCloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Create presentation content on the go. Duplicate your screen on the tablet device on to any monitor or projector for wider audience with out any wires. Write on your tablet over your presentation slides and save your points!</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-md-offset-2">
						<div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="images/services/ser8.png" alt=""/></a>
								</div>
								<div class="media-body">
									<h4>Interactive Presentation Platform</h4>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">MIonCloud</a></li>
									</ul>
								</div>
							</div><!--/.media -->
							<p>Admin can create promotion content separately for each user, add images, videos, text and maps and publish it to individual users
                                The users can view and interact the content published to them on various touch and web devices </p>
						</div>
					</div>
				</div>	<!--/.row-->
			</div>
           </div>
</asp:Content>


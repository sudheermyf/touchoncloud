﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
    <style>
        .para-italic {
            color: #ff7505;font-style:italic;
        }
    </style>
     <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
     <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <div class="center wow fadeInDown" style="background:#12a5f4;box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
          <div id="background-wrap" style="height: 0px;">
                <div class="x4">
                    <div class="cloud"></div>
                </div>
            </div>
            <h2 style="color:#FFF;">Our Products</h2>
            <p class="lead" style="color:#FFF;">Our Products are delivered by a team of highly experienced & passionate Engineers</p>
        </div>

    <section class="service-item" style="background:rgba(164,164,164,0.1); padding-top:3%;">
        <div class="container">           
            <div class="row">
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">                      
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">PORTFOLIO</h3>
                            <p class="para-italic">our work</p>
                            <p style="text-align: justify;">
                                From, creating a cloud platform to developing your online branding, real time product catalogue and a holistic social media integration – we have products to do it all. And more importantly do it quickly.
                                </p>
                            <p style="text-align: justify;">
                                We provide the complete solution – Hardware on a one-time-purchase model and cloud based software as a service at a monthly subscription model. Based on your requirement these software applications can either be <u style="color:#12a5f4">‘on-cloud’ or ‘Self-hosted’</u>                                
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"   src="images/products/prd1-l.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Interactive Promotion Platform</h3>
                            <p class="para-italic">Software as a Service</p>
                            <p class="para-italic">Create a Common Promotion Platform for your organization</p>
                            

                            <p style="text-align: justify;">
                                Have control over your marketing and promotion collateral. This platform enables you manage content – create, modify and give access to specific users.
                                Showcase the content on touch screens, monitors, projectors or even as downloadable documents.
                            </p>
                            <p><b>Implementation time:</b>  48 hours  </p>
                            <p><b>Payment type: </b>  Pay as you go – diminishing monthly subscriptions </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd2-l.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Web Portal or Mobile App</h3>
                            <p class="para-italic">Product as a Service</p>
                            <p class="para-italic">List your entire product range on our e-commerce platform</p>                            

                            <p style="text-align: justify;">
                               Display all your products for the world to see on an ecommerce platform completely integrated with the social media to improve your branding and sales – both online and offline
                            </p>
                            <p>Also manage your inventory across each of your store locations</p>
                            <p><b>Implementation time:</b>  7 days  </p>
                            <p><b>Payment type: </b>  Pay as you go – diminishing monthly subscriptions </p>

                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd3-l.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Cloud Touch Floor Stand</h3>
                            
                            <p class="para-italic">Product as a Service</p>
                            <p  class="para-italic">Interactive touch screens displaying your products</p>
                            <p style="text-align: justify;">
                               Engage your customers through touch screen interfaces of various sizes – 21” to 84”. Let them browse through your entire product range across all your stores and help your sales team to understand your customers better
                                    Available in various aesthetic designs – floor stand, table top, wall mount, 
                            </p>
                            <p><b>Implementation time:</b>  15-20 days </p>
                            <p><b>Payment type: </b> Hardware: One time Purchase | Software: Pay as you go – diminishing monthly subscriptions</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd4-l.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Sales & Presentation Tab</h3>
                            <p class="para-italic">Product as a Service</p>
                            <p class="para-italic">Wirelessly mirror your touch tab with any monitor or projector</p>
                            
                            <p style="text-align: justify;">
                                Empower your sales team, give them sleek 8”-11” tablet devices to show case your products and services to your potential customers. They can express interest and block your inventory, giving you real time market data and customer contacts
                            </p>
                            <p><b>Implementation time:</b>   15 days </p>
                            <p><b>Payment type: </b> One time Purchase with annual maintenance</p>                        
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd5-l.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Interactive Glass</h3>
                            <p class="para-italic">Product as a Service</p>
                            <p style="text-align: justify;">
                                Attract crowd, by transforming your store glass into a touch screen! Showcase all your products and improve footfall and brand image. No wires, no obstruction – your glass will remain see-through
                            </p>
                            <p><b>Implementation time:</b> 15-20 days</p>
                            <p><b>Payment type: </b> Hardware: One time Purchase | Software: Pay as you go – diminishing monthly subscriptions</p>                        
                        </div>
                    </div>
                </div>
                   
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

  

</asp:Content>


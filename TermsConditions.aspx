﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TermsConditions.aspx.cs" Inherits="TermsConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="center wow fadeInDown" style="background: #12a5f4; padding: 2%; box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
        <div id="background-wrap" style="height: 0px;">
            <div class="x4">
                <div class="cloud"></div>
            </div>
        </div>
        <h2 style="color: #fff;">TERMS & CONDITIONS</h2>
    </div>

    <div class="container">
        <!-- Terms and Conditions -->
        <div class="skill-wrap clearfix">
            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>I. Introduction</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    A.  You (“User”) are required to read and accept all of the terms and conditions laid down in this Terms and 
                    Conditions (“Terms” or “Agreement”) and the linked Privacy Policy, before you may use www.TOUCHONCLOUD.com (hereinafter referred 
                    to as “Site” or “we” or “our”).<br />

                    B.  These Terms are effective upon acceptance and governs the relationship between the User and TOUCHONCLOUD.com, c/o 
                    E Retail Solutions Private Limited (“Company” and also includes its affiliates and subsidiaries, jointly and severally)
                     and also the use of the website including wireless services or systems (“Website” or “Site”) including the sale and supply of 
                    any Products on the site. If the Terms conflict with any other document, the Terms will prevail for the purposes of usage of the
                     Site. If the User does not agree to be bound by these Terms and the Privacy Policy, the User will not be permitted to use the 
                    Site in any way.

                    <br />
                    C.  The Company provides the goods and services to the User subject to the Terms.<br />
                    D.  All information accessed or viewed by the User is considered confidential and is for only authorized personal or business purposes.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>II. Acceptance of Terms</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    A. The Website is the property of one or more of the member companies and/or affiliates of the Company. 
                    By linking, referencing, using or accessing the Website, the User agrees to these Terms, including agreeing to indemnify 
                    and hold harmless the Company from all claims of any nature arising from the access and use of these websites by the User.
                     These Terms may be changed at any time at the sole discretion of the Company. These Terms pertain to all Websites of the 
                    Company, including websites owned, operated or sponsored by any of the subsidiaries or affiliates of the Company.
                    <br />
                    B. Please read these Terms carefully. These Terms, as modified or amended from time to time, are a binding contract between the 
                    Company and the User. If the User visits, uses, or shops at the Site (or any future site operated by the Company), the User a
                    ccepts these Terms. In addition, when the User uses any current or future Services of the Company or visits or purchases from 
                    any business affiliated with the Company, the User will also be subject to the guidelines and conditions applicable to such 
                    service or merchant.   
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>To read the complete Terms, please see below.</h4>
                <h4>1. Description of Services</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    In the Site, we provide touch based solutions based on user requirement.                    
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>2. General</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    These Terms sets forth the terms and conditions that apply to the use of the Site by the User. 
                    By using this Site, the User agrees to comply with all the Terms hereof. The right to use the Site is 
                    personal to the User and is not transferable to any other person or entity. The User shall be responsible for 
                    protecting the confidentiality of their password(s), if any. The User acknowledges that, although the internet is 
                    often a secure environment, sometimes there are interruptions in service or events that are beyond the control of the Company, 
                    and the Company shall not be responsible for any data lost while transmitting information on the internet. 
                    While it is the Company’s objective to make the Site accessible 24 hours a day, 7 days a week, the Site may be unavailable 
                    from time to time for any reason including, without limitation, routine maintenance. The User understands and acknowledges 
                    that due to the circumstances both within and outside the control of the Company, access to the Site may be interrupted, 
                    suspended or terminated from time to time. The Company shall have the right at any time to change or discontinue any aspect or 
                    feature of the Site, including, but not limited to, content, hours of availability and equipment needed for access or use. 
                    Further, the Company shall also have the right to discontinue disseminating any portion of information or change any category 
                    of information or eliminate any transmission method and may change transmission speeds or other signal characteristics. 
                    
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>3. Modified Terms</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The Company reserves the right at all times to discontinue or modify any of its Terms and/or the Privacy Policy as may be 
                    deemed necessary or desirable without prior notification to the User. Such changes may include, among other things, the adding 
                    of certain fees or charges. Further, if the Company makes any changes to the Terms and Privacy Policy and the User continues to
                     use the Site, the User is impliedly agreeing to the Terms and Privacy Policy expressed therein. Any such changes, deletions or
                     modifications shall be effective immediately upon the Company’s posting thereof. Any use of the Site by the User after such 
                    notice shall be deemed to constitute acceptance by the User of such modifications.
                  
                </p>
            </div>


            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>4. Equipment</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The User shall be responsible for obtaining and maintaining telephone, computer hardware, internet devices and other equipment                     needed for access to and use of the Site and all charges related thereto. The Company shall not be liable for any damages 
                    to the User's equipment resulting from the use of the Site.                   
                </p>
            </div>


            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>5. License and Site Access</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The Company grants the User limited license to access and use the Site and the Service. This license does not include:<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;a. any downloading or copying of account information for the benefit of another vendor or any other third party or for self;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;b. caching, unauthorized hypertext links to the Site and the framing of any Content available through the Site uploading, posting, or transmitting any content that the User does not have a right to make available (such as the intellectual property of another party);<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;c. uploading, posting, or transmitting any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; <br />
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;d. any action that imposes or may impose (in the Company’s sole discretion) an unreasonable or disproportionately large load on the Company’s infrastructure; or<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;e. any use of data mining, robots, or similar data gathering and extraction tools.
                    <br />
                    The User is not permitted to bypass any measures used by the Company to prevent or restrict access to the Site. Any unauthorized use by 
                    the User shall result in termination of the permission or license granted to the User by the Company.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>6. Membership Eligibility</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    (a) Use of the Site is available only to persons who can form legally binding contracts under applicable law. Persons who are                     "incompetent to contract" within the meaning of the Indian Contract Act, 1872 including un-discharged insolvents,                     and any users suspended or removed from the system by the company for any reason. are not eligible to use the Site.                     If the user is a minor i.e. under the age of 18 years but is at least 13 years of age you may use this Site only under the                     supervision of an adult being either a parent or legal guardian who agrees to be bound by these Terms & Conditions.                     If your age is below that of 18 years your parents or legal guardians can transact on behalf of you if they are registered users.
                    <br />
                    (b) Users may not have more than one account. Maintaining more than one account by a user shall amount to fraudulent act on part of the user                     and attract actions against such user in accordance with the terms of Clause 11 below.
                    <br />
                    (c) Additionally, users are prohibited from selling, trading, or otherwise transferring your account to another party.                     If you do not qualify, you may not use the Service or the Site.
                    <br />
                    (d) The Company owns no responsibility in any manner over any dispute arising out of transactions by any third party using your account/e-mail                     provided by you to the Company or payments made by your credit card by any third party.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>7. Disclaimer of Warranty</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    All content, products, and services on the site, or obtained from a website to which the Site is linked ( "Linked Site") are provided to                     the user "as is" without warranty of any kind either express or implied including, but not limited to, the implied warranties of merchantability and                     fitness for a particular purpose, title, non-infringement, security or accuracy. All warranties, if any, relating to the Product and Services would                     be provided by the manufacturer/supplier of such Product and not by the Company. Any claim in relation to the same should be raised against respective                     manufacturer/supplier and not the Company in any case whatsoever. The Company does not endorse and is not responsible for:
                    <br />
                    (a) the accuracy or reliability of any opinion, advice or statement made through the site by any party other than the Company;
                    <br />
                    (b) any content provided on linked sites or                    
                    <br />
                    (c) the capabilities or reliability of any Product or Service obtained from a Linked Site. Other than as                     required under applicable consumer protection law, under no circumstance will the Company be liable for any loss or damage caused by the User's reliance                     on information obtained through the Site or a Linked Site, or the User's reliance on any Product or Service obtained from a Linked Site.                     It is the responsibility of the User to evaluate the accuracy, completeness or usefulness of any opinion, advice or other content available through 
                    the Site, or obtained from a Linked Site.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>8. Limitation of Liability</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The User expressly understands and agrees that the Company and its subsidiaries, affiliates, officers, employees, agents, partners and licensors shall                     not be liable to the User for any direct, indirect, incidental, special, consequential or exemplary damages, including, but not limited to,                     damages for loss of profits, opportunity, goodwill, use, data or other intangible losses (even if the Company has been advised of the possibility                     of such damages), resulting from use of the Site, sale and supply of goods, content or any related/unrelated services and other services offered on
                     the Website from time to time.
                    <br />
                    The Company may provide, or third parties may provide, links to other World Wide websites. However, since the Company does not have control over these sites,                     the User agrees that the Company is not responsible for any liability resulting from the access or use of sites not owned by the Company and                     that the Company is not responsible for any content, advertising, product, or any other matter or issue, available through the World Wide Web,                     whether linked to, or from, websites of the Company.
                    Further, the User agrees that the Company shall not be responsible or liable, directly or indirectly, for any damage or loss caused,                     or alleged to be caused by or in connection with the use of websites which the Company does not own.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>9. Indemnity</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The User agrees to indemnify and hold the Company (including its officers, directors, agents, subsidiaries, affiliates, joint ventures, and employees)                     harmless from any claim or demand, including but not limited to reasonable attorneys’ fees, or arising out of or related to the User’s breach of the Terms,                     or violation of any law or the rights of a third party.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>10. Electronic Communication</h4>
            </div>

            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    When the User uses the Site or sends e-mails to the Company, the User is communicating with the Company electronically. The User consents to receive                     communications from the Company electronically. The Company will communicate with the User by e-mail, SMS or by posting notices on the Site.                     The User agrees that all agreements, notices, disclosures and other communications that the Company provides electronically shall satisfy any legal
                     requirement that such communications be in writing.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>11. Site-Provided Email and Postings</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    (a) The Site may provide users with the ability to send email messages to other users and non-users and to post messages on the Site and also on Facebook.                     The Company and the User is under no obligation to review any messages, information or content (“Postings”) posted on the Site by users and assumes                     no responsibility or liability relating to any such Postings. Notwithstanding the above, the Company may from time to time monitor the Postings on the                     Site and may decline to accept and/or remove any e-mail or Postings.
                    <br />
                    (b) The User understands and agrees not to use any functionality provided by the Site to post content or initiate communications that contain:
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(i) Any unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful, racially, ethnically or otherwise objectionable                     material of any kind, including, but not limited to, any material which encourages conduct that would constitute a criminal offense, give rise to civil                     liability or otherwise violate any applicable local, state, national or international law;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(ii) Advertisements or solicitations of any kind;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(iii) Impersonate others or provide any kind of false information;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(iv) Personal information such as messages which state phone numbers, account numbers, addresses, or employer references;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(v) Messages by non-spokesperson employees of the Company purporting to speak on behalf of the Company or containing confidential information or expressing                     opinions concerning the Company;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(vi) Messages that offer unauthorized downloads of any copyrighted or private information;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(vii) Multiple messages placed within individual folders by the same user restating the same point;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(viii) Chain letters of any kind;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;(ix) Identical (or substantially similar) messages to multiple recipients advertising any product or service, expressing a political or other similar                     message, or any other type of unsolicited commercial message. This prohibition includes but is not limited to:                    
                    (a) Using the Company Website invitations to send messages to people who don’t know you or who are unlikely to recognize you as a known contact;
                    (b) Using the Site to connect to people who don’t know you and then sending unsolicited promotional messages to those direct connections without 
                    their permission; and                   
                    (c) Sending messages to distribution lists, newsgroup aliases, or group aliases.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>12. Links</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The Site or third parties may provide links to other World Wide Web sites or resources.                     The User acknowledges and agrees that the Company is not responsible for the availability of such external sites or resources, and does not endorse and                     is not responsible or liable for any content, advertising, products or other materials on or available from such sites or resources. The User further                     acknowledges and agrees that the Company shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused                     by or in connection with use of or reliance on any such content, goods or services available on or through any such site or resource.
                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>13. Modifications and Notification of Changes</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    The Company reserves the right to make changes to the Site, related policies and agreements, this Terms and the Privacy Policy at any time.                     If the Company makes a material modification to the Terms, it will notify the User by:                                         &nbsp;&nbsp;&nbsp;&nbsp;i. sending an email to the email address associated with the User account. 
                    The Company is not responsible for the User’s failure to receive an e-mail due to the actions of the User’s ISP or any e-mail filtering service;                     therefore, the User should add the Site to the list of domains approved to send you e-mail (commonly known as “whitelist”);
                    &nbsp;&nbsp;&nbsp;&nbsp;ii. displaying a prominent announcement above the text of the Terms or the Privacy Policy, as appropriate, for thirty (30) days, is deemed sufficient                     notification, of such changes. After the notice of a modification to the Terms or the Privacy Policy has been posted for 30 days, the notice will                     be removed and a brief description of the modification and the date that it went into effect will be placed in the Modifications section at the end                     of the Terms. Should the User wish to terminate the User account due to a modification to the Terms or the Privacy Policy, the User may do so by                     sending an email with the subject line “Termination” to the following email address: <hyperlink>info@TOUCHONCLOUD.com.</hyperlink>
                    If the User chooses to continue using the Site, the User agrees that doing so will be deemed acceptance of the new Terms or Privacy 
                    Policy, as relevant. It is the user’s moral obligation to provide us the updated email address.

                </p>
            </div>

            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>14. Trademarks, Copyrights and other Intellectual Property Rights</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">
                    (i) Trademarks: The trademarks, logos and service marks ("Marks") displayed on the Site are the property of the Company and other respective persons.                     Users are prohibited from using any Marks for any purpose including, but not limited to use as metatags on other pages or sites on the World Wide Web                     without the written permission of the Company or such third party which may own the Marks.
                    <br />
                    (ii) Trademarks that are located within or on the Site or a website otherwise owned or operated in conjunction with the Company shall not be deemed to be in                     the public domain but rather the exclusive property of the Company, unless such site is under license from the trademark owner thereof, in which case such license is for the                     exclusive benefit and use of the Company, unless otherwise stated.
                    <br />
                    (iii) Copyright: All information and content including any software programs available on or through the Site ("Content") is protected by copyright.                     Users are prohibited from modifying, copying, distributing, transmitting, displaying, publishing, selling, licensing, creating derivative works or using any                     Content available on or through the Site for commercial or public purposes.
                    <br />
                    (iv) The Site contains copyrighted material, trademarks, trade secrets, patents, and other proprietary information, including, but not limited to, text,                     software, photos, video, graphics, music, sound, and the entire contents of the Company protected by copyright as a collective work under the applicable 
                    copyright laws.
                    <br />
                    (v) The Company owns a copyright in the selection, coordination, arrangement and enhancement of such content, as well as in the content original to it.
                    <br />
                    (vi) The User is not permitted to modify, publish,download, transmit, participate in the transfer or sale, create derivative works, or in any way exploit,                     any of the content, in whole or in part. However, the User may only download / print / save / share details of the product which the user already owns.                     Except as otherwise expressly stated under copyright law, no copying, redistribution, retransmission, publication or commercial exploitation of downloaded                     material without the express permission of the Company and the copyright owner is permitted. If copying, redistribution or publication of copyrighted                     material is permitted, no changes in or deletion of author attribution, trademark legend or copyright notice shall be made.
                    <br />
                    (vii) The User acknowledges that he/she/it does not acquire any ownership rights by downloading copyrighted material.
                    <br />
                    (viii) The User shall not upload post or otherwise make available on the Site any material protected by copyright, trademark, patent, proprietary rights                     or any other intellectual property rights, without the express permission of the owner of the copyright, trademark, proprietary right or other intellectual property rights.
                    <br />
                    (ix) The Company does not have any express burden or responsibility to provide the User with indications, markings or anything else that may aid the User in 
                        determining whether the material in question is copyrighted or trademarked.
                    <br />
                    (x) The User shall be solely liable for any damage resulting from any infringement of copyrights, trademarks, proprietary rights or any other harm resulting                     from such a submission.
                    <br />
                    (xi) By submitting material to any public area of the Site, the User warrants that the owner of such material has expressly granted the Company the                     royalty-free, perpetual, irrevocable, non-exclusive right and license to use, reproduce, modify, adapt, publish, translate and distribute such material                     (in whole or in part) worldwide and/or to incorporate it in other works in any form, media or technology now known or hereafter developed for the full term of any copyright that may exist in such material.
                    <br />
                    (x) User hereby grants the Company, the right to edit, copy, publish and distribute any material made available on the Site by the User.
                    <br />
                    (xi) Copyright Policy: The Company reserves the right to remove from the Site, any content that is alleged to infringe someone's copyright.
                </p>
            </div>


            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>15. Refund Clause</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">                    
                    For the purpose of this clause account credit means credit issued to the User account for the entire purchase amount less the shipping costs.                     The User account credit can be used towards the next purchase with the Company.
                </p>
            </div>


            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>16. Termination and Survival of Terms after Agreement Ends</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">                    
                    The Company may terminate this Agreement at any time. Without limiting the foregoing, the Company shall have the right to immediately terminate any                     passwords or accounts of the User in the event of any conduct by the User which the Company, in its sole discretion, considers to be unacceptable, or in                     the event of any breach by the User of this Agreement. Notwithstanding any other provisions of the Terms, or any general legal principles to the contrary,                     any provision of the Terms that imposes or contemplates continuing obligations on a party will survive the expiration or termination of the Terms.
                </p>
            </div>

            
            <div class="center wow fadeInDown" style="text-align: left;">
                <h4>17. Miscellaneous</h4>
            </div>
            <div class="center wow fadeInDown">
                <p class="lead" style="text-align: justify; font-size: 10pt;">                    
                    (i) If any of these conditions are deemed invalid, void, or for any reason unenforceable, the parties agree that the court should endeavour to give effect                     to the parties’ intentions as reflected in the provision, and the unenforceable condition shall be deemed severable and shall not affect the validity and enforceability of                     any remaining condition.
                    <br />
                    (ii) The Terms and the relationship between the User and the Company will be governed by the laws as applicable in India.
                    <br />
                    (iii) Any disputes will be handled in the competent courts of Hyderabad, Andhra Pradesh, India.
                    <br />
                    (iv) The failure of the Company to act with respect to a breach by the User or others does not waive its right to act with respect to subsequent or similar breaches.
                    <br />
                    (v) Except as otherwise, expressly provided in these Terms, there shall be no third-party beneficiaries to the same.                     These Terms constitutes the entire agreement between the User and the Company and governs the User’s use of the Site, superseding any prior agreements
                     between the User and the Company with respect to the Site.
                </p>
            </div>

        </div>
        <!--/.our-Terms and Conditions-->


    </div>
    <!--/.container-->
</asp:Content>


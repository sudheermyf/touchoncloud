﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

  <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .col-xs-12{
             box-shadow: 0px 1px 12px rgba(238, 238, 238,0.6);
            background: rgba(238, 238, 238,0.6);          
            border: 1px solid transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    <section id="main-slider" class="no-margin">
        <div id="background-wrap">
            <div class="frame">
                <div class="plane-container">                  
                    <a href="http://customer.io/" target="_blank">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="1131.53px" height="379.304px" viewBox="0 0 1131.53 379.304" enable-background="new 0 0 1131.53 379.304"
                            xml:space="preserve" class="plane">                        
                        </svg></a>
                </div>
            </div>
            <div class="clouds">

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="762px"
                    height="331px" viewBox="0 0 762 331" enable-background="new 0 0 762 331" xml:space="preserve" class="cloud big front slowest">
                    <path fill="Transparent" stroke="#FFF" stroke-width="3" d="M715.394,228h-16.595c0.79-5.219,1.201-10.562,1.201-16c0-58.542-47.458-106-106-106
c-8.198,0-16.178,0.932-23.841,2.693C548.279,45.434,488.199,0,417.5,0c-84.827,0-154.374,65.401-160.98,148.529
C245.15,143.684,232.639,141,219.5,141c-49.667,0-90.381,38.315-94.204,87H46.607C20.866,228,0,251.058,0,279.5
S20.866,331,46.607,331h668.787C741.133,331,762,307.942,762,279.5S741.133,228,715.394,228z" />
                </svg>
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="762px"
                    height="331px" viewBox="0 0 762 331" enable-background="new 0 0 762 331" xml:space="preserve" class="cloud distant smaller">
                    <path fill="#FFFFFF" d="M715.394,228h-16.595c0.79-5.219,1.201-10.562,1.201-16c0-58.542-47.458-106-106-106
c-8.198,0-16.178,0.932-23.841,2.693C548.279,45.434,488.199,0,417.5,0c-84.827,0-154.374,65.401-160.98,148.529
C245.15,143.684,232.639,141,219.5,141c-49.667,0-90.381,38.315-94.204,87H46.607C20.866,228,0,251.058,0,279.5
S20.866,331,46.607,331h668.787C741.133,331,762,307.942,762,279.5S741.133,228,715.394,228z" />
                </svg>

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="762px"
                    height="331px" viewBox="0 0 762 331" enable-background="new 0 0 762 331" xml:space="preserve" class="cloud small slow">
                    <path fill="#FFFFFF" d="M715.394,228h-16.595c0.79-5.219,1.201-10.562,1.201-16c0-58.542-47.458-106-106-106
c-8.198,0-16.178,0.932-23.841,2.693C548.279,45.434,488.199,0,417.5,0c-84.827,0-154.374,65.401-160.98,148.529
C245.15,143.684,232.639,141,219.5,141c-49.667,0-90.381,38.315-94.204,87H46.607C20.866,228,0,251.058,0,279.5
S20.866,331,46.607,331h668.787C741.133,331,762,307.942,762,279.5S741.133,228,715.394,228z" />
                </svg>

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="762px"
                    height="331px" viewBox="0 0 762 331" enable-background="new 0 0 762 331" xml:space="preserve" class="cloud distant super-slow massive">
                    <path fill="#FFFFFF" d="M715.394,228h-16.595c0.79-5.219,1.201-10.562,1.201-16c0-58.542-47.458-106-106-106
c-8.198,0-16.178,0.932-23.841,2.693C548.279,45.434,488.199,0,417.5,0c-84.827,0-154.374,65.401-160.98,148.529
C245.15,143.684,232.639,141,219.5,141c-49.667,0-90.381,38.315-94.204,87H46.607C20.866,228,0,251.058,0,279.5
S20.866,331,46.607,331h668.787C741.133,331,762,307.942,762,279.5S741.133,228,715.394,228z" />
                </svg>

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="762px"
                    height="331px" viewBox="0 0 762 331" enable-background="new 0 0 762 331" xml:space="preserve" class="cloud slower">
                    <path  fill="Transparent" stroke="#FFF" stroke-width="3" d="M715.394,228h-16.595c0.79-5.219,1.201-10.562,1.201-16c0-58.542-47.458-106-106-106
c-8.198,0-16.178,0.932-23.841,2.693C548.279,45.434,488.199,0,417.5,0c-84.827,0-154.374,65.401-160.98,148.529
C245.15,143.684,232.639,141,219.5,141c-49.667,0-90.381,38.315-94.204,87H46.607C20.866,228,0,251.058,0,279.5
S20.866,331,46.607,331h668.787C741.133,331,762,307.942,762,279.5S741.133,228,715.394,228z" />
                </svg>

            </div>
            <div id="circleslider4">
                <div class="viewport">
                    <ul class="overview">
                        <li><a href="#" rel="group" class="animationdelay">                             
                            <img src="images/mt.png" /></a></li>

                        <li><a href="#" rel="group" class="animationdelay1">                            
                             <img src="images/tp.png"/>                            
                            </a></li>

                        <li><a href="#" rel="group" class="animationdelay2">
                            <img src="images/c.png" /></a></li>

                        <li><a href="#" rel="group" class="animationdelay3">                            
                            <img src="images/tk.png" /></a></li>

                        <li><a href="#" rel="group" class="animationdelay4">
                            <img src="images/tw.png" /></a></li>
                    </ul>
                </div>

                <div class="dot"></div>
                <div class="overlay"></div>
                <div class="thumb"></div>


            </div>

        </div>

        <div style="float: right;position: absolute;top: 0;right: 0;z-index: 9999;">
            <img src="images/cloud-host.png" />
        </div>
    </section>

 
    <%--<section id="recent-works" style="margin-top:3%;">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Recent Works</h2>                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/item1.jpg" alt=""/>
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <a class="preview" href="images/portfolio/full/item1.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i>View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/item2.jpg" alt=""/>
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <a class="preview" href="images/portfolio/full/item2.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i>View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/item3.jpg" alt=""/>
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <a class="preview" href="images/portfolio/full/item3.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i>View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive" src="images/portfolio/recent/item4.jpg" alt=""/>
                        <div class="overlay">
                            <div class="recent-work-inner">
                                <a class="preview" href="images/portfolio/full/item4.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i>View</a>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
           
        </div>
        
    </section>--%>
    <!--/#recent-works-->

    <section id="services" class="service-item">
        <div class="container">
            <div class="center wow fadeInDown" style="margin-top:2%;">
                <h2 style="color:#4e4e4e;">Our Products</h2>
            </div>

            <div class="row">
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">                      
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">PORTFOLIO</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">our work</p>
                            <p style="text-align: justify; font-size: 10pt;">
                                From, creating a cloud platform to developing your online branding, real time product catalogue and a holistic social media integration – we have products to do it all. And more importantly do it quickly.
                                </p>
                            <p style="text-align: justify; font-size: 10pt;">
                                We provide the complete solution – Hardware on a one-time-purchase model and cloud based software as a service at a monthly 
                                subscription model.
                                <br />
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="hypid" runat="server" Text="see more..." ToolTip="our work..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive" src="images/products/prd1.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Interactive Promotion Platform</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">Software as a Service</p>
                            <p style="text-align: justify; font-size: 10pt;">
                                Have control over your marketing and promotion collateral. This platform enables you manage content – create, modify and give access to specific users.
                                <br />
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="HyperLink1" runat="server" Text="see more..." ToolTip="Interactive Promotion Platform..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd2.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Web Portal or Mobile App</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">Product as a Service</p>
                            <p style="text-align: justify; font-size: 10pt;">
                               Display all your products for the world to see on an ecommerce platform completely integrated with the social media to improve your 
                                branding and sales – both online and offline.
                                &nbsp;&nbsp;
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="HyperLink2" runat="server" Text="see more..." ToolTip="Web Portal or Mobile App..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd3.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Cloud Touch Floor Stand</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">Product as a Service</p>
                            <p style="text-align: justify; font-size: 10pt;">
                                Engage your customers through touch screen interfaces of various sizes – 21” to 84”. Let them browse through your entire product.
                                &nbsp;&nbsp;
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="HyperLink3" runat="server" Text="see more..." ToolTip="Cloud Touch Floor Stand..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd4.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Sales & Presentation Tab</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">Product as a Service</p>
                            <p style="text-align: justify; font-size: 10pt;">
                                Empower your sales team, give them sleek 8”-11” tablet devices to show case your products and services to your potential customers.
                                &nbsp;&nbsp;
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="HyperLink4" runat="server" Text="see more..." ToolTip="Sales & Presentation Tab..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <img class="img-responsive"  src="images/products/prd5.png" alt="" />
                        <div class="media-body">
                            <h3 class="media-heading" style="font-weight: 600; margin: 5px 0;">Interactive Glass</h3>
                            <p style="margin: 0px;color: #ff7505;font-style:italic;">Product as a Service</p>
                            <p style="text-align: justify; font-size: 10pt;">
                                Attract crowd, by transforming your store glass into a touch screen! Showcase all your products and improve footfall and brand image.
                                &nbsp;&nbsp;
                                <asp:HyperLink NavigateUrl="~/Products.aspx" ID="HyperLink5" runat="server" Text="see more..." ToolTip="Interactive Glass..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
                   
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <!--/#services-->
  <section id="feature" class="transparent-bg">
        <div class="container">          
            <div class="clients-area center wow fadeInDown">
                <h2>Who is saying what</h2>
            </div>

            <div class="row">
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-1.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Lot Mobiles </span> </h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-2.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Chandana Brothers</span></h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-3.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Pothys</span> </h4>
                    </div>
                </div>
           </div>

        </div>
    </section>
    <!--/#feature-->



</asp:Content>


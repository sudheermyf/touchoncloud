﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SuccessStories.aspx.cs" Inherits="SuccessStories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="center wow fadeInDown" style="background: #12a5f4; padding: 2%; box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
        <div id="background-wrap" style="height: 0px;">
            <div class="x4">
                <div class="cloud"></div>
            </div>
        </div>
        <h2 style="color: #fff;">Success Stories</h2>
        <p class="lead" style="color: #FFF;"></p>
    </div>

    <section id="feature" class="transparent-bg">
        <div class="container">          
            <div class="clients-area center wow fadeInDown">
                <h2>Who is saying what</h2>
            </div>

            <div class="row">
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-1.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Lot Mobiles </span> </h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-2.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Chandana Brothers</span></h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="images/client-3.png" class="img-circle" alt=""/>
                        <h3></h3>
                        <h4><span>- Pothys</span> </h4>
                    </div>
                </div>
           </div>

        </div>
    </section>

</asp:Content>


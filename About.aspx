﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="center wow fadeInDown" style="background: #12a5f4; padding: 1%; box-shadow: rgba(5, 150, 255, 0.298039) 0px 12px 19px inset, rgba(3, 142, 216, 0.398039) 0px 1px 6px">
        <div id="background-wrap" style="height: 0px;">
            <div class="x4">
                <div class="cloud"></div>
            </div>
        </div>
        <h2 style="color: #fff;">A LITTLE BIT</h2>
        <p class="lead" style="color: #FFF;font-size: 20pt;">about us</p>
    </div>

    <section id="about-us">

        <div class="container">

            <!-- Our Skill -->
            <div class="skill-wrap clearfix">

                <div class="center wow fadeInDown">
                    <p class="lead" style="border-bottom: 1px #ececec solid; padding: 1%;font-weight: 600;">
                        E-Retails Solutions was established to make high-end technology a reality and accessible to everyone. Our vision is to revolutionize 
                        consumer experience - make it more fun, social and engaging across industries.
                    </p>
                </div>

                <div class="col-sm-12 map-content">
                    <ul class="row" style="list-style-type: none;">
                          <li class="col-sm-6">
                            <img src="images/abt.jpg" alt=""/>
                        </li>

                        <li class="col-sm-6">                       

                                <p>
                                    E-Retails solutions operates throudh its brands "myfashions" and "touchoncloud".
                                    Myfashions is a complete end to end branding and selling platform developed exclusively for the fashion retail industry on the cloud.
                                    "Mypromotions" extends this platform to all marking endeavours across all industries by adding centralized push promotions, events and social media features.
                                </p>
                            </li>
                         <li class="col-sm-6">            
                                <p>                                
                                    "Touchoncloud" extends both the platforms for various touch screen devices. Our focus remains on customer satisfaction - on time, on budget,
                                    SLA based post sale service and through customizations.
                                </p>
                        </li>


                        <li class="col-sm-6">
                           <p>
                               Inspired by James Bond and other Sci-Fi movies, what started as a proof of concept by a couple of techies is now a full fledged limited firm with 
                                backing from promoters of Moldtek group of companies, an INR 350 crore Public Limited company.
                           </p>
                        </li>
                        
                         <li class="col-sm-6">            
                             <div></div>
                            <p>                                
                                With expertise in 6 Technology platforms, the best of product designers and over 30 engineers from top colleges like IITs and NITs, we built products 
                                that will revolutionize the retail industry. Including sales and customer support, ours is a 60-member team which ensures the best possible service 
                                for our clients.
                            </p>
                        </li>                      
                        
                        <li class="col-sm-6" style="width: 15%; margin-top:5%;">
                            <img src="images/myf.png" alt=""/>
                        </li>
                        <li class="col-sm-6" style="width: 15%; margin-top:5%;">
                            <img src="images/toc.png" alt=""/>
                        </li>
                        <li class="col-sm-6" style="width: 15%; margin-top:5%;">
                            <img src="images/mi1.png" alt=""/>
                        </li>

                        

                      <%--  <li class="col-sm-6">
                           <ul class="row" style="list-style-type: none;margin-right: -10px;margin-left: -44px;">
                               <li style="float:left;width: 30%;">
                                   <img src="images/logo.png" alt=""/>
                               </li>
                               <li style="float:left;width: 30%;">
                                   <img src="images/logo.png" alt=""/>

                               </li>
                               <li style="float:left;width: 26%;">
                                   <img src="images/logo.png" alt=""/>

                               </li>
                           </ul>
                        </li>--%>
                    </ul>

                </div>                                  
            </div>
            <!--/.our-skill-->

             <div class="center wow fadeInDown">
                    <p class="lead" style="font-style: oblique; text-align: justify;font-size: 20pt;color: #ff7505;">
                       Sell more, sell better.
                    </p>
                </div>   

        </div>
        <!--/.container-->
    </section>
    <!--/about-us-->
</asp:Content>

